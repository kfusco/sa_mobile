<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description>ItemDetail Call
Store 205 Item 104602750</description>
   <name>apiItemDetail</name>
   <tag></tag>
   <elementGuidId>17acdbfd-b165-4a67-9cbc-b1105622a8ac</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://staging-storeservices.twmlabs.com/item/develop-item-service/v1/ItemApi/search/205/104602750?isItemCode=true&amp;includeBasicInfo=true&amp;includeVintage=true&amp;includeLocation=true&amp;includeRetails=true&amp;includeInventory=true&amp;includeOrdering=true&amp;includeSales=true&amp;includeRating=true&amp;includeReview=true&amp;includeSeason=true&amp;includeLTS=false&amp;includeUpc=true</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>''</defaultValue>
      <description></description>
      <id>55fbfc7f-1048-4021-900f-8d313aa82078</id>
      <masked>false</masked>
      <name>variable</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
