import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import java.lang.ProcessEnvironment$StringValues as StringValues

WebUI.callTestCase(findTestCase('Login/Autologin_906_User'), [:], FailureHandling.STOP_ON_FAILURE)

'Enter an item name'
Mobile.setText(findTestObject('ItemSearch/android.widget.EditText0 - Search_Box'), 'merl', 0)

'Tap Search button'
Mobile.tap(findTestObject('ItemSearch/android.view.ViewGroup15_SeaarchButton'), 0)

not_run: Mobile.getText(findTestObject('ItemSearch/android.widget.TextView4 - NORMAL STOCK (120)'), 0)

'Verify Normal Stock not =0'
Mobile.verifyNotEqual(findTestObject('ItemSearch/android.widget.TextView4 - NORMAL STOCK (120)'), findTestObject('ItemSearch/android.widget.TextView4 - NORMAL STOCK (0)'))

'Verify All Items not = 0'
Mobile.verifyNotEqual(findTestObject('ItemSearch/android.widget.TextView5 - ALL ITEMS (999)'), findTestObject('ItemSearch/android.widget.TextView5 - ALL ITEMS (0)'))

