import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('Acceptance/Autologin_906_User'), [:], FailureHandling.STOP_ON_FAILURE)

'Verify Search field'
Mobile.verifyElementExist(findTestObject('SEARCH/android.widget.EditText0 - Search.Input'), 0)

'Enter an item name'
Mobile.setText(findTestObject('SEARCH/android.widget.EditText0 - Search.Input'), 'armani pinot grigio valdadige', 0)

'Tap Search button'
Mobile.tap(findTestObject('SEARCH/android.widget.TextView3 -  SEARCH'), 0)

'Verify Item Details page'
Mobile.checkElement(findTestObject('ItemDetail/android.widget.TextView0 - ITEM DETAILS'), 0)

