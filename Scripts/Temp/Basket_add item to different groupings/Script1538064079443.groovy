import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Load, login, exact searh'
WebUI.callTestCase(findTestCase('Acceptance/Exact_Wine'), [:], FailureHandling.STOP_ON_FAILURE)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView10 - plus'), 0)

'Tap to select a grouping\t\n'
Mobile.tap(findTestObject('Basket/android.widget.TextView18 -  - select grouping'), 0)

'Select Pull as a grouping'
Mobile.tap(findTestObject('Basket/android.widget.TextView22 - Pull'), 0)

'Tap the plus button to add the item to basket'
Mobile.tap(findTestObject('Basket/android.widget.ImageView10 - plus'), 0)

'Verify Qty of the item'
Mobile.verifyElementHasAttribute(findTestObject('Basket/android.widget.EditText0 - Qty 1 field'), '1', 1, FailureHandling.STOP_ON_FAILURE)

'Verify the Qty of line items in basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView24 - 1 Qty on basket'), '1')

'Tap to select a grouping\t\n'
Mobile.tap(findTestObject('Basket/android.widget.TextView18 -  - select grouping'), 0)

'Select Report as a grouping'
Mobile.tap(findTestObject('Basket/android.widget.TextView23 - Report'), 0)

'Tap the plus button to add the item to basket'
Mobile.tap(findTestObject('Basket/android.widget.ImageView10 - plus'), 0)

'Verify Qty of the item'
Mobile.verifyElementHasAttribute(findTestObject('Basket/android.widget.EditText0 - Qty 1 field'), '1', 1, FailureHandling.STOP_ON_FAILURE)

'Verify the Qty of line items in basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView16 - 2_basket Qty on footer'), '2')

'Tap the basket icon to navigate to the basket\t'
Mobile.tap(findTestObject('Basket/android.widget.TextView25 - BASKET icon'), 0)

'Verify Basket screen is displayed.'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView0 - BASKET title'), 'BASKET')

'Verify current basket screen'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView1 - CURRENT BASKET'), 'CURRENT BASKET')

'Verify the Item was added to the basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView8 - Armani Pinot Grigio Valdadige_vrify item name'), 
    'Armani Pinot Grigio Valdadige')

'Verify the item is added to the basket with the correc grouping'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView10 - Pull_verify grouping'), 'Pull')

'Verify the Item was added to the basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView8 - Armani Pinot Grigio Valdadige_vrify item name'), 
    'Armani Pinot Grigio Valdadige')

'Verify the item is added to the basket with the correc grouping'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView10 - Report_verify group'), 'Report')

Mobile.closeApplication()

