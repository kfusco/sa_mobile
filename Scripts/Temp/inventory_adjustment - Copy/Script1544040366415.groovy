import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('Temp/login_for_test'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('SEARCH/android.widget.TextView5 - ALL ITEMS (0)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('SEARCH/android.widget.TextView4 - NORMAL STOCK (0)'), 0, FailureHandling.STOP_ON_FAILURE)

'Enter Text to search'
Mobile.setText(findTestObject('SEARCH/android.widget.EditText0 - Search.Input'), 'Beluga Transatlantic Vodka', 0)

'Select Search'
Mobile.tap(findTestObject('SEARCH/android.view.ViewGroup14-SearchButton'), 0)

'Verify Product Name'
Mobile.verifyElementText(findTestObject('Items/Beluga_Transatlantic/android.widget.TextView2 - BELUGA TRANSATLANTIC VODKA'), 
    'BELUGA TRANSATLANTIC VODKA')

Mobile.verifyElementExist(findTestObject('Items/Beluga_Transatlantic/android.view.ViewGroup20-InventoryToggle'), 0)

'Select Inventory Corkscrew\n'
Mobile.tap(findTestObject('Inventory/InventoryAdjustment/InventoryToggle'), 0)

Mobile.delay(2)

'Select New Adjustment\n\t\t\t\t\t\t'
Mobile.tap(findTestObject('Inventory/InventoryAdjustment/android.view.ViewGroup11-NewAdjustmentTab'), 20, FailureHandling.STOP_ON_FAILURE)

'Wait for New Adjustment Tab'
Mobile.waitForElementPresent(findTestObject('Inventory/InventoryAdjustment/android.widget.TextView7 - New Physical Inventory Qty'), 
    0, FailureHandling.STOP_ON_FAILURE)

'Set Cases to >0'
Mobile.sendKeys(findTestObject('Inventory/InventoryAdjustment/Inventory-Cases', [('Case') : '']), GlobalVariable.Case, FailureHandling.STOP_ON_FAILURE)

'Set Units >0'
Mobile.sendKeys(findTestObject('Inventory/InventoryAdjustment/Inventory-Units', [('Units') : '']), GlobalVariable.Units, 
    FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Inventory/InventoryAdjustment/android.view.ViewGroup17 -reasons-dropdown'), 0)

'Select Bad Count\n'
Mobile.tap(findTestObject('Inventory/InventoryAdjustment/android.widget.CheckedTextView1 - Bad Count'), 0)

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Inventory/InventoryAdjustment/new-adjustment_actions_dropdown'), 0)

Mobile.setText(findTestObject('Inventory/InventoryAdjustment/android.widget.EditText2-Notes'), 'No Physical Change Bad Count-No Action Taken', 
    0)

'Select Finalize'
Mobile.tap(findTestObject('Inventory/InventoryAdjustment/android.view.ViewGroup19-FinalizeButton'), 0)

'Select Confirm'
Mobile.tap(findTestObject('Inventory/InventoryAdjustment/android.widget.TextView3 - CONFIRM'), 0)

'Verify Product Name'
Mobile.verifyElementText(findTestObject('Items/Beluga_Transatlantic/android.widget.TextView2 - BELUGA TRANSATLANTIC VODKA'), 
    'BELUGA TRANSATLANTIC VODKA')

'Close SAM'
not_run: Mobile.closeApplication()

