import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('Acceptance/Autologin_906_User'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('SEARCH/android.view.ViewGroup14-SearchButton'), 0)

'Enter Text to search'
Mobile.setText(findTestObject('SEARCH/android.widget.EditText0 - Search...'), 'Beluga Transatlantic Vodka', 0)

'Select Search'
Mobile.tap(findTestObject('SEARCH/android.widget.TextView3 -  SEARCH'), 0)

'Verify Product Name'
Mobile.verifyElementText(findTestObject('Items/Beluga_Transatlantic/android.widget.TextView2 - BELUGA TRANSATLANTIC VODKA'), 
    'BELUGA TRANSATLANTIC VODKA')

Mobile.scrollToText('Item History', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('ItemHistory/android.widget.TextView20 - Item History'), 0)

Mobile.verifyElementText(findTestObject('ItemHistory/android.widget.TextView8 - WEEK'), 'WEEK')

