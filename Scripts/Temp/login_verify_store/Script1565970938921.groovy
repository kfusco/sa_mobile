import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

Mobile.startApplication('/proof/android/release/app-release.apk', true)

Mobile.verifyElementText(findTestObject('SplashScreen/android.widget.TextView1 - Retry'), 'Retry')

'Retry Button '
Mobile.tap(findTestObject('SplashScreen/android.widget.TextView1 - Retry'), 0)

'Manager Name\n'
Mobile.setText(findTestObject('Login/android.widget.EditText0 - Username'), 'samautotest', 0)

'Manager Pass'
Mobile.setText(findTestObject('Login/android.widget.EditText1 - Password'), 'grapes1', 0)

'Login Button'
Mobile.tap(findTestObject('Login/android.view.ViewGroup8 - Login Button'), 0)

'Select_Store'
Mobile.tap(findTestObject('Login/android.view.ViewGroup5 - Store Select'), 0)

'Scroll to Store 917'
Mobile.scrollToText('917-Total Wine - Wellington', FailureHandling.STOP_ON_FAILURE)

'Select Store 917'
Mobile.tap(findTestObject('Login/android.widget.CheckedTextView6 - 917-Total Wine - Wellington'), 0)

'Save Store'
Mobile.tap(findTestObject('Login/android.view.ViewGroup7 - Save Store'), 0)

'Wait for Toast message to dismiss\n'
Mobile.delay(4)

'Open Hamburger'
Mobile.tap(findTestObject('Header/android.view.ViewGroup8-Hamburger'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Header/android.widget.TextView3 - Additional Info'), 0)

'Verify Store Name'
Mobile.verifyElementText(findTestObject('Login/LoginAdditionalInfo/android.widget.TextView12 - Store Name Wellington'), 
    'Store Name: Wellington', FailureHandling.STOP_ON_FAILURE)

'Close Hamburger'
Mobile.tap(findTestObject('Header/android.view.ViewGroup8-Hamburger'), 0, FailureHandling.STOP_ON_FAILURE)

