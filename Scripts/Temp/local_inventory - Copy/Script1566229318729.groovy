import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

'Login Store 917-Wellington'
Mobile.callTestCase(findTestCase('Temp/login_for_test'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Enter Item Tname\n'
Mobile.setText(findTestObject('SEARCH/android.widget.EditText0 - Search.Input'), 'Beluga Transatlantic Vodka', 0)

'Saerch for Entered Item\n\t'
Mobile.tap(findTestObject('SEARCH/android.widget.TextView3 -  SEARCH'), 0)

'Select Local Inventory Icon'
Mobile.tap(findTestObject('Inventory/LocalInventory/LocalInventoryStoreFront'), 0)

'Verify Panel Local Store Inventory'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView2 - Local Area Inventory'), 'Local Area Inventory: ')

'Verify Radius 50 Miles'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView3 - 50 Mi'), '50 Mi')

'Verify 9 Stores Listed'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView5 - 9'), '9')

'Select Store 906 Show moew'
Mobile.tap(findTestObject('Inventory/LocalInventory/android.view.ViewGroup12 - 906 -show-more-toggle'), 0, FailureHandling.STOP_ON_FAILURE)

'Verify Show more- map Icon'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.ImageView1 - Map Icon'), 0, FailureHandling.STOP_ON_FAILURE)

'Verify 906 Address'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.TextView13 - 850 North Congress Ave.'), 
    0, FailureHandling.STOP_ON_FAILURE)

'Verify 906 Address'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.TextView14 - Boynton Beach FL 33426'), 
    0, FailureHandling.STOP_ON_FAILURE)

'Close Store 906 Show more'
Mobile.tap(findTestObject('Inventory/LocalInventory/android.view.ViewGroup12 - 906 -show-more-toggle'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('931-Aventura')

'Verify Store 931 Shown'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.TextView11 - 931-Aventura'), 0)

'Select Store 931 Show more'
Mobile.tap(findTestObject('Inventory/LocalInventory/android.view.ViewGroup12 - 931-show-more-toggle'), 0, FailureHandling.STOP_ON_FAILURE)

'Verify 931Address'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.TextView13 - 19925 Biscayne Blvd.'), 0, 
    FailureHandling.STOP_ON_FAILURE)

'Verify 931Address'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.TextView14 - Miami FL 33180'), 0, FailureHandling.STOP_ON_FAILURE)

'Close Store 931'
Mobile.tap(findTestObject('Inventory/LocalInventory/android.view.ViewGroup12 - 931-show-more-toggle'), 0, FailureHandling.STOP_ON_FAILURE)

'Close Local Invemtory'
Mobile.tap(findTestObject('Inventory/LocalInventory/android.view.ViewGroup5 -Inventory CLose X'), 0, FailureHandling.STOP_ON_FAILURE)

'Verify Return to Item Details'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/LocalInventoryStoreFront'), 0)

'Close SAM'
Mobile.closeApplication()

