import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

'Login Store 917-Wellington'
Mobile.callTestCase(findTestCase('Temp/login_for_test'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

'Enter Item Tname\n'
Mobile.setText(findTestObject('ItemSearch/Item Search/android.widget.EditText0 - Search...'), 'Beluga Transatlantic Vodka', 
    0)

'Search for Entered Item\n\t'
Mobile.tap(findTestObject('ItemSearch/Item Search/android.widget.TextView3 -  SEARCH btn'), 0)

'Select Local Inventory Icon'
Mobile.tap(findTestObject('Inventory/LocalInventory/LocalInventoryStoreFront'), 0)

'Verify Panel Local Store Inventory'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView2 - Local Area Inventory'), 'Local Area Inventory: ')

'Verify Radius 50 Miles'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView3 - 50 Mi'), '50 Mi')

'Verify 9 Stores Listed'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView5 - 9'), '9')

'Verify Slider'
Mobile.verifyElementExist(findTestObject('Inventory/LocalInventory/android.widget.SeekBar - Slider'), 0, FailureHandling.STOP_ON_FAILURE)

'Set Slider to 100 (95 miles)'
Mobile.setSliderValue(findTestObject('Inventory/LocalInventory/android.widget.SeekBar - Slider'), 100, 13, FailureHandling.CONTINUE_ON_FAILURE)

'Verify Slider Changed to 95 Miles'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView3 - 95 Mi'), '95 Mi', FailureHandling.STOP_ON_FAILURE)

'Verify Number of Stores'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView5 - 12 Number of Stores'), '12', 
    FailureHandling.STOP_ON_FAILURE)

'Set Slider to 0 Miles'
Mobile.setSliderValue(findTestObject('Inventory/LocalInventory/android.widget.SeekBar - Slider'), 1, 3, FailureHandling.CONTINUE_ON_FAILURE)

'Verify 0 mile Message'
Mobile.verifyElementText(findTestObject('Inventory/LocalInventory/android.widget.TextView6 - There are no nearby stores with this item within this distance.'), 
    'There are no nearby stores with this item within this distance.', FailureHandling.STOP_ON_FAILURE)

'Close SAM'
Mobile.closeApplication()

