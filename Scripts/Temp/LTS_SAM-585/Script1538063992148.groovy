import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.callTestCase(findTestCase('Temp/login_for_test'), [:], FailureHandling.STOP_ON_FAILURE)

'Verify Search field'
Mobile.verifyElementExist(findTestObject('ItemSearch/Item Search/android.widget.EditText0 - Search...'), 0)

'Enter an item name'
Mobile.setText(findTestObject('ItemSearch/Item Search/android.widget.EditText0 - Search...'), 'firebrand merlot', 0)

'Tap Search button'
Mobile.tap(findTestObject('ItemSearch/Item Search/android.widget.TextView3 -  SEARCH btn'), 0)

'Verify Item Details page'
Mobile.checkElement(findTestObject('ItemDetail/android.widget.TextView0 - ITEM DETAILS'), 0)

'Verify LTS badge in the perist zone'
Mobile.verifyElementExist(findTestObject('ItemDetail/LTS badge and price/android.widget.ImageView3 - LTS badge'), 0)

'Tap the Item Image'
Mobile.tap(findTestObject('ItemDetail/LTS badge and price/android.widget.ImageView4 - Item image'), 0)

'Verify LTS badge on expand image\n\n'
Mobile.verifyElementExist(findTestObject('ItemDetail/LTS badge and price/android.widget.ImageView3 - LTS badge on img'), 
    0)

'Tap "X" to collapse Image'
Mobile.tap(findTestObject('ItemDetail/LTS badge and price/android.widget.ImageView2 - img dismiss btn'), 0)

'Verify price is displayed\n\n'
Mobile.verifyElementExist(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView6 - 8.99_price on the page'), 
    0)

'Verify price is correct'
Mobile.verifyElementText(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView6 - 8.99_price on the page'), 
    '$8.99')

'Tap the price position \n'
Mobile.tapAndHold(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView6 - 8.99_price on the page'), 0, 
    0)

'Popup: verify Original price field\n'
Mobile.verifyElementExist(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView1 - 9.99_original on popup'), 
    0)

'Popup: verify Original price data\n'
Mobile.verifyElementText(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView1 - 9.99_original on popup'), 
    '$9.99')

'Popup: verify LTS field\t\n\n'
Mobile.verifyElementExist(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView2 - LTS on popup'), 0)

'Popup: verify LTS field data\n\n'
Mobile.verifyElementText(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView3 - 8.99 (0904 - 1031)_LTS on popup'), 
    '$8.99 (09/04 - 10/31)')

'Verify small LTS sign under price'
Mobile.verifyElementExist(findTestObject('ItemDetail/LTS badge and price/android.widget.TextView7 - LTS small'), 0)

not_run: Mobile.closeApplication()

