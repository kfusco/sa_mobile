import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('Temp/login_for_test'), [:], FailureHandling.STOP_ON_FAILURE)

'Verify Search field'
Mobile.verifyElementExist(findTestObject('SEARCH/android.widget.EditText0 - Search.Input'), 0)

'Enter an item name'
Mobile.setText(findTestObject('SEARCH/android.widget.EditText0 - Search.Input'), 'Kendall Jackson Cabernet', 0)

'Tap Search button'
Mobile.tap(findTestObject('SEARCH/android.view.ViewGroup14-SearchButton'), 0)

'Verify Corkscrew'
Mobile.verifyElementExist(findTestObject('Vintage/android.view.ViewGroup12 - vintage corkscrew'), 0, FailureHandling.STOP_ON_FAILURE)

' verify year badge exists'
Mobile.verifyElementText(findTestObject('Vintage/android.widget.TextView3 -  Vintage Year 2015'), '2015')

Mobile.tap(findTestObject('Vintage/android.view.ViewGroup11 - Vintage Toggle'), 0)

Mobile.swipe(0, 0, 0, 0)

