import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

Mobile.callTestCase(findTestCase('Acceptance/Autologin_906_User'), [:], FailureHandling.STOP_ON_FAILURE)

'Set Search Text'
Mobile.setText(findTestObject('ItemSearch/android.widget.EditText0 - Search_Box'), 'Beluga', 0)

'Search'
Mobile.tap(findTestObject('ItemSearch/android.view.ViewGroup15_SeaarchButton'), 0)

'Select Beluga Noble'
Mobile.tap(findTestObject('SEARCH/Beluga/android.widget.TextView11 - Beluga Noble Vodka  750ml'), 0)

'Return to Search'
Mobile.tap(findTestObject('Footer/android.view.ViewGroup19'), 0)

'Verify All Items Count'
Mobile.verifyElementExist(findTestObject('Items/Beluga_Transatlantic/android.widget.TextView5 - ALL ITEMS (9)'), 0, FailureHandling.STOP_ON_FAILURE)

'Scroll to Beluga Transatlantic'
Mobile.scrollToText('Beluga Transatlantic Vodka')

'Select Beluga Transatlantic'
Mobile.tap(findTestObject('Items/Beluga_Transatlantic/android.widget.TextView16 - Beluga Transatlantic Vodka  750ml'), 0)

'Return to Search'
Mobile.tap(findTestObject('Footer/footer.search_ico'), 0)

Mobile.closeApplication()

