import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.callTestCase(findTestCase('Acceptance/Autologin_906_User'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Feedback/android.widget.ImageView6'), 0)

Mobile.setText(findTestObject('Feedback/android.widget.EditText0 - Please enter your name...'), 'TWM Develop', 0)

Mobile.tap(findTestObject('Feedback/android.widget.Spinner0'), 0)

Mobile.tap(findTestObject('Feedback/android.widget.CheckedTextView2 - General Feedback'), 0)

Mobile.setText(findTestObject('Feedback/android.widget.EditText1'), 'DEV TEST', 0)

Mobile.tap(findTestObject('Feedback/android.view.ViewGroup14'), 0)

