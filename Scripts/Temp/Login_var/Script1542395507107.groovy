import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

'Load the App\n'
Mobile.startApplication('/proof/android/Release/app-release.apk', true)

'Tap Retry'
Mobile.tap(findTestObject('SplashScreen/android.widget.TextView1 - Retry'), 0, FailureHandling.CONTINUE_ON_FAILURE)

'Enter Username'
Mobile.setText(findTestObject('Login/android.widget.EditText0 - Username'), 'kfusco1', 0)

'Enter Password'
Mobile.setText(findTestObject('Login/android.widget.EditText1 - Password'), 'grapes1', 0)

'Tap Login button'
Mobile.tap(findTestObject('Login/android.view.ViewGroup8 - Login Button'), 0)

'Scroll '
Mobile.scrollToText('917-Total Wine - Wellington', FailureHandling.STOP_ON_FAILURE)

'Select a store'
Mobile.tap(findTestObject('Login/android.widget.CheckedTextView6 - 917-Total Wine - Wellington'), 0)

'Save the store'
Mobile.tap(findTestObject('Login/android.widget.TextView5 - SAVE'), 0)

'Wait 5 sec to dismiss a confirmation popup\n'
Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

'Tap hamburger btn'
Mobile.tap(findTestObject('Login/android.view.ViewGroup3 - hamburger btn'), 0)

'Tap Additional information under hamb menu\n'
Mobile.tap(findTestObject('Login/android.widget.TextView7 - Additional Info'), 0)

'Verify the selected store'
Mobile.verifyElementExist(findTestObject('Login/android.widget.TextView13 - Store Number 917'), 0)

'Close menu'
Mobile.tap(findTestObject('Login/android.view.ViewGroup3 - hamburger btn'), 0)

Mobile.closeApplication()

