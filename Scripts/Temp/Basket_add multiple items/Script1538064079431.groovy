import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Load, login, exact searh'
WebUI.callTestCase(findTestCase('Acceptance/Exact_Wine'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(1)

Mobile.verifyElementExist(findTestObject('Basket/android.widget.ImageView -PLUS_ICON'), 0, FailureHandling.STOP_ON_FAILURE)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView -PLUS_ICON'), 0)

'Tap to select a grouping\t\n'
Mobile.tap(findTestObject('Basket/android.widget.TextView18 -  - select grouping'), 0)

'Select Pull as a grouping'
Mobile.tap(findTestObject('Basket/android.widget.TextView22 - Pull'), 0)

'Tap the plus button to add the item to basket'
Mobile.tap(findTestObject('Basket/android.widget.ImageView -PLUS_ICON'), 0)

'Tap the Search icon on footer'
Mobile.tap(findTestObject('Basket/android.widget.TextView9 - SEARCH'), 0)

'Clear the search field'
Mobile.tap(findTestObject('Basket/android.widget.TextView2 - _clear search line'), 0)

'Enter Search string to the search filed'
Mobile.setText(findTestObject('Basket/android.widget.EditText0 - Search filed...'), '14 hands chardonnay', 0)

'Tap the Search button'
Mobile.tap(findTestObject('Basket/android.widget.TextView3 -  SEARCH button'), 0)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView -PLUS_ICON'), 0)

'Tap to select a grouping\t\n'
Mobile.tap(findTestObject('Basket/android.widget.TextView18 -  - select grouping'), 0)

Mobile.scrollToText('INV ADJ', FailureHandling.CONTINUE_ON_FAILURE)

'Select INV ADJ as a grouping'
Mobile.tap(findTestObject('Basket/android.widget.TextView25 - INV ADJ'), 0)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView_DELETE'), 0)

'Verify Qty on Basket icon\n'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView16 - 2_basket Qty on footer'), '2')

'Tap the basket icon to navigate to the basket\t'
Mobile.tap(findTestObject('Basket/android.widget.TextView25 - BASKET icon'), 0)

'Verify Basket screen is displayed.'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView0 - BASKET title'), 'BASKET')

'Verify current basket screen'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView1 - CURRENT BASKET'), 'CURRENT BASKET')

'Verify the Item was added to the basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView8 - 14 Hands Chardonnay_verify item in basket'), 
    '14 Hands Chardonnay')

'Verify the item is added to the basket with the correc grouping'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView10 - INV ADJ_verify group in basket'), 'INV ADJ')

'Verify the Item was added to the basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView8 - Armani Pinot Grigio Valdadige_vrify item name'), 
    'Armani Pinot Grigio Valdadige')

'Verify the item is added to the basket with the correc grouping'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView15 - Pull_verify in basket'), 'Pull')

Mobile.closeApplication()

