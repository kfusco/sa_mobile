import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Load, login, exact searh'
WebUI.callTestCase(findTestCase('Acceptance/Exact_Wine'), [:], FailureHandling.STOP_ON_FAILURE)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView11-PLUS'), 0)

'Tap to select a grouping\t\n'
Mobile.tap(findTestObject('Basket/android.widget.TextView18 -  - select grouping'), 0)

'Select Pull as a grouping'
Mobile.tap(findTestObject('Basket/android.widget.TextView22 - Pull'), 0)

'Tap the plus button to add the item to basket'
Mobile.tap(findTestObject('Basket/android.widget.ImageView11-PLUS'), 0)

'Tap the basket icon on footer'
Mobile.tap(findTestObject('Basket/android.widget.ImageView6 - Basket footer icon'), 0)

'Tap the save basket tab'
Mobile.tap(findTestObject('Basket/android.widget.TextView19 - SAVE Basket'), 0)

'Enter a basket name'
Mobile.setText(findTestObject('Basket/android.widget.EditText0 - Enter Basket Name'), 'MergeBasket', 0)

'Check permanent'
Mobile.checkElement(findTestObject('Basket/android.widget.TextView1 - Make Permanent text'), 0)

'Tap Confirm'
Mobile.tap(findTestObject('Basket/android.widget.TextView6 - CONFIRM save basket'), 0)

'Tap Basket icon'
Mobile.tap(findTestObject('Basket/android.widget.TextView25 - BASKET icon'), 0)

'Navigate to Manage Baskets screen'
Mobile.tap(findTestObject('Basket/android.widget.TextView2 - MANAGE BASKETS'), 0)

'Verify MergeBasket saved successfully'
Mobile.verifyElementExist(findTestObject('Basket/android.widget.TextView9 - MergeBasket_verify basket'), 0)

'Tap the Search icon on footer'
Mobile.tap(findTestObject('Basket/android.widget.TextView9 - SEARCH'), 0)

'Clear the search field'
Mobile.tap(findTestObject('Basket/android.widget.TextView2 - _clear search line'), 0)

'Enter Search string to the search filed'
Mobile.setText(findTestObject('Basket/android.widget.EditText0 - Search filed...'), 'Zima', 0)

'Tap the Search button'
Mobile.tap(findTestObject('Basket/android.widget.TextView3 -  SEARCH button'), 0)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView11-PLUS'), 0)

'Tap to select a grouping\t\n'
Mobile.tap(findTestObject('Basket/android.widget.TextView18 -  - select grouping'), 0)

'Select Report as a grouping'
Mobile.tap(findTestObject('Basket/android.widget.TextView23 - Report grouping'), 0)

'Tap the Plus button\t\n'
Mobile.tap(findTestObject('Basket/android.widget.ImageView11-PLUS'), 0)

'Tap the basket icon on footer'
Mobile.tap(findTestObject('Basket/android.widget.ImageView6 - Basket footer icon'), 0)

'Navigate to Current basket'
Mobile.tap(findTestObject('Basket/android.widget.TextView1 - CURRENT BASKET'), 0)

'Tap the save basket tab'
Mobile.tap(findTestObject('Basket/android.widget.TextView14 - SAVE Zima'), 0)

'Enter a basket name'
Mobile.setText(findTestObject('Basket/android.widget.EditText0 - Enter Basket Name'), 'MergeBasket2', 0)

'Check permanent'
Mobile.checkElement(findTestObject('Basket/android.widget.TextView1 - Make Permanent text'), 0)

'Tap Confirm'
Mobile.tap(findTestObject('Basket/android.widget.TextView6 - CONFIRM save basket'), 0)

'Tap Basket icon'
Mobile.tap(findTestObject('Basket/android.widget.TextView25 - BASKET icon'), 0)

'Navigate to Manage Baskets screen'
Mobile.tap(findTestObject('Basket/android.widget.TextView2 - MANAGE BASKETS'), 0)

'Verify MergeBasket saved successfully'
Mobile.verifyElementExist(findTestObject('Basket/android.widget.TextView9 - MergeBasket2_verify basket'), 0)

'Check a basket to merge'
Mobile.checkElement(findTestObject('Basket/android.view.ViewGroup29-MB_checkbox2'), 0)

'Check a basket to merge'
Mobile.checkElement(findTestObject('Basket/android.view.ViewGroup19-MB2_checkbox2'), 0)

'Tap MERGE button'
Mobile.tap(findTestObject('Basket/android.view.ViewGroup55_MERGE button2'), 2)

'Enter a new basket name'
Mobile.setText(findTestObject('Basket/android.widget.EditText0 - Enter Basket Name_merged baskets'), 'MergeBaskets3', 0)

'Tap CONFIRM button'
Mobile.tap(findTestObject('Basket/android.widget.TextView3 - CONFIRM merge'), 0)

'Verify the Qty of items in the basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView14 - QTY 2_verify in merged basket'), 'QTY: 2')

'Verify the Qty of items in the basket'
Mobile.verifyElementText(findTestObject('Basket/android.widget.TextView9 - MergeBaskets3_verify basket'), 'MergeBaskets3')

Mobile.setText(findTestObject(null), '', 0)

Mobile.closeApplication()

