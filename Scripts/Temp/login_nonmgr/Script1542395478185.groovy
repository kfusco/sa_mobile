import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

'Start SAM'
Mobile.startApplication('/proof/android/release/app-release.apk', true)

'Verify Retry Exists'
Mobile.verifyElementExist(findTestObject('SplashScreen/android.widget.TextView1 - Retry'), 0)

'Select Retry'
Mobile.tap(findTestObject('SplashScreen/android.widget.TextView1 - Retry'), 0)

'Enter Username'
Mobile.setText(findTestObject('Login/android.widget.EditText0 - Username'), 'kfusco1', 0)

'Enter Password'
Mobile.setText(findTestObject('Login/android.widget.EditText1 - Password'), 'grapes1', 5)

'Select Login Button'
Mobile.tap(findTestObject('Login/android.view.ViewGroup8 - Login Button'), 0)

'Find Store 917 - Wellington'
Mobile.scrollToText('917-Total Wine - Wellington', FailureHandling.STOP_ON_FAILURE)

'Select Store 917 - Wellington'
Mobile.tap(findTestObject('Login/android.widget.CheckedTextView6 - 917-Total Wine - Wellington'), 0)

'Save Store'
Mobile.tap(findTestObject('Login/android.view.ViewGroup7 - Save Store'), 0)

Mobile.verifyElementVisible(findTestObject('ItemSearch/Item Search/android.widget.TextView3 -  SEARCH btn'), 0)

Mobile.delay(2)

