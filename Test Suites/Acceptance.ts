<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Acceptance</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8e8a7960-04a7-476a-909c-0271069ef462</testSuiteGuid>
   <testCaseLink>
      <guid>c5a7ecb6-4e4c-4257-9f9b-6abba970ce58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Acceptance/Autologin_906_User</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ae59814-77ab-4165-b29f-439b9e3850ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Acceptance/Exact_Wine</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5079b303-9d3b-436c-8f22-3ba07fd4586d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Acceptance/local_inventory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c9d9094-0a8e-4a01-ac19-c67b957a7c34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Acceptance/ViewBaskets</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
