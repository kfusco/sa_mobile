<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inventory</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1ed73678-f181-49c0-8550-6eb5a234fdf4</testSuiteGuid>
   <testCaseLink>
      <guid>936d2eba-36d5-4e20-a3d0-ba2f19aa001e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Temp/inventory_adjustment (1)</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>19a4ca7d-0f0d-4259-b3f5-58ec766801e9</id>
         <iterationEntity>
            <iterationType>RANGE</iterationType>
            <value>31-32</value>
         </iterationEntity>
         <testDataId>Data Files/InvAdjust/InvValues</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>19a4ca7d-0f0d-4259-b3f5-58ec766801e9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>InvCase</value>
         <variableId>b302a9f1-5992-4b0e-9fe4-57b6e1eb5d9f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>19a4ca7d-0f0d-4259-b3f5-58ec766801e9</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>InvUnit</value>
         <variableId>d7fc577f-0c74-4c34-a1fa-3d65a389a523</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
